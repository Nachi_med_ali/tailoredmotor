"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', 'heuro.views.home', name='home'),
    url(r'^download/$', 'heuro.views.download', name='download'),
    url(r'^draw/$', 'heuro.views.draw', name='draw'),
    url(r'^gensolid/$', 'heuro.views.gensolid', name='gensolid'),
    url(r'^downloadstp/$', 'heuro.views.downloadstp', name='downloadstp'),

]
