from __future__ import print_function
from django.shortcuts import render
import os
from OCC.STEPControl import STEPControl_Reader
from OCC.Display.WebGl import x3dom_renderer
from wsgiref.util import FileWrapper
from django.http import HttpResponse
from OCC.BRepAlgoAPI import BRepAlgoAPI_Fuse, BRepAlgoAPI_Cut
from OCC.AIS import AIS_MultipleConnectedInteractive, AIS_Shape
from OCC.TopoDS import TopoDS_Shape
from django.http import HttpResponse, Http404, JsonResponse
import json

from OCC.Display.SimpleGui import *
from OCC.BRepPrimAPI import BRepPrimAPI_MakeCylinder
from OCC.gp import gp_Ax1, gp_Pnt, gp_Dir, gp_Trsf, gp_Vec
from OCC.BRepBuilderAPI import BRepBuilderAPI_Transform
from math import pi
from .models import updat


from OCC.STEPControl import STEPControl_Writer, STEPControl_AsIs
from OCC.Interface import Interface_Static_SetCVal
from OCC.IFSelect import IFSelect_RetDone
# Create your views here.

def home(request):
	
	path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/stp/motor.stp"
	# print path
	step_reader = STEPControl_Reader()
	step_reader.ReadFile(path)
	step_reader.TransferRoot()
	block_cylinder_shape = step_reader.Shape()
	my_renderer = x3dom_renderer.X3DomRenderer()
	box = my_renderer.DisplayShape(block_cylinder_shape)
	context = {"box" : box,}
	template = "index.html"
	return render(request, template, context)

def draw(request):
	if request.method == 'POST':
				
		if request.POST.get("rcm") == "":
			rcm = 13
		else:
			rcm = int(request.POST.get("rcm"))

		if request.POST.get("lcm") == "":
			lcm = 15
		else:
			lcm = int(request.POST.get("lcm"))

		if request.POST.get("lb") == "":
			lb = 20
		else:
			lb = int(request.POST.get("lb"))


		if request.POST.get("lc") == "":
			lc = 5
		else:
			lc = int(request.POST.get("lc"))

		if request.POST.get("rc") == "":
			rc = 15
		else:
			rc = int(request.POST.get("rc"))

		my_renderer = x3dom_renderer.X3DomRenderer()
	
		center = BRepPrimAPI_MakeCylinder(rcm, lcm).Shape()
		#rayon, longueur
		cyl_1 = BRepPrimAPI_MakeCylinder(rc, lc).Shape()
		trns = gp_Trsf()
		trns.SetTranslation(gp_Vec(0,0,-lc))

		gen_1 = BRepBuilderAPI_Transform(cyl_1, trns).Shape()

		cyl_2 = BRepPrimAPI_MakeCylinder(rc, lc).Shape()

		trou = BRepPrimAPI_MakeCylinder(1, lc).Shape()
		tr = gp_Trsf()
		tr.SetTranslation(gp_Vec(0,rc - 2,0))
		gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
		result = BRepAlgoAPI_Cut(cyl_2, gen_3).Shape()

		tr.SetTranslation(gp_Vec(rc -2 ,0,0))
		gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
		result = BRepAlgoAPI_Cut(result, gen_3).Shape()

		tr.SetTranslation(gp_Vec(-rc +2,0,0))
		gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
		result = BRepAlgoAPI_Cut(result, gen_3).Shape()

		tr.SetTranslation(gp_Vec(0,-rc +2,0))
		gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
		result = BRepAlgoAPI_Cut(result, gen_3).Shape()


		trns = gp_Trsf()
		trns.SetTranslation(gp_Vec(0,0,lcm))

		gen_2 = BRepBuilderAPI_Transform(result, trns).Shape()



		rou = BRepPrimAPI_MakeCylinder(2, lb).Shape()
		trns = gp_Trsf()
		trns.SetTranslation(gp_Vec(0,0,lb-5))

		gen_3 = BRepBuilderAPI_Transform(rou, trns).Shape()

		result = BRepAlgoAPI_Fuse(center, gen_1).Shape()
		result = BRepAlgoAPI_Fuse(gen_2, result).Shape()
		result = BRepAlgoAPI_Fuse(gen_3, result).Shape()

		dis = my_renderer.DisplayShape(result)
		form = updat()
	
		context = {"dis": dis,"form" : form,}
		template = "draw.html"
		return render(request, template, context)
	else:
			

		my_renderer = x3dom_renderer.X3DomRenderer()
		
		center = BRepPrimAPI_MakeCylinder(13, 15).Shape()

		cyl_1 = BRepPrimAPI_MakeCylinder(15, 5).Shape()
		trns = gp_Trsf()
		trns.SetTranslation(gp_Vec(0,0,-5))

		gen_1 = BRepBuilderAPI_Transform(cyl_1, trns).Shape()

		cyl_2 = BRepPrimAPI_MakeCylinder(15, 5).Shape()

		trou = BRepPrimAPI_MakeCylinder(1, 5).Shape()
		tr = gp_Trsf()
		tr.SetTranslation(gp_Vec(0,14,0))
		gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
		result = BRepAlgoAPI_Cut(cyl_2, gen_3).Shape()

		tr.SetTranslation(gp_Vec(14,0,0))
		gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
		result = BRepAlgoAPI_Cut(result, gen_3).Shape()

		tr.SetTranslation(gp_Vec(-14,0,0))
		gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
		result = BRepAlgoAPI_Cut(result, gen_3).Shape()

		tr.SetTranslation(gp_Vec(0,-14,0))
		gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
		result = BRepAlgoAPI_Cut(result, gen_3).Shape()


		trns = gp_Trsf()
		trns.SetTranslation(gp_Vec(0,0,+10))

		gen_2 = BRepBuilderAPI_Transform(result, trns).Shape()



		rou = BRepPrimAPI_MakeCylinder(2, 20).Shape()
		trns = gp_Trsf()
		trns.SetTranslation(gp_Vec(0,0,+15))

		gen_3 = BRepBuilderAPI_Transform(rou, trns).Shape()

		result = BRepAlgoAPI_Fuse(center, gen_1).Shape()
		result = BRepAlgoAPI_Fuse(gen_2, result).Shape()
		result = BRepAlgoAPI_Fuse(gen_3, result).Shape()

		dis = my_renderer.DisplayShape(result)
		form = updat()
		
		context = {"dis": dis,"form" : form,}
		template = "draw.html"
		return render(request, template, context)

def gensolid(request):

	if request.method == 'POST':
		
		dat = json.loads(request.body)
		# print dat["lc"]
		result = drawsolid(int(dat["lc"]))
		return HttpResponse(json.dumps(result), content_type='application/json')

def downloadstp(request):

		
	center = BRepPrimAPI_MakeCylinder(13, 15).Shape()

	cyl_1 = BRepPrimAPI_MakeCylinder(15, 5).Shape()
	trns = gp_Trsf()
	trns.SetTranslation(gp_Vec(0,0,-5))

	gen_1 = BRepBuilderAPI_Transform(cyl_1, trns).Shape()

	cyl_2 = BRepPrimAPI_MakeCylinder(15, 5).Shape()

	trou = BRepPrimAPI_MakeCylinder(1, 5).Shape()
	tr = gp_Trsf()
	tr.SetTranslation(gp_Vec(0,14,0))
	gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
	result = BRepAlgoAPI_Cut(cyl_2, gen_3).Shape()

	tr.SetTranslation(gp_Vec(14,0,0))
	gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
	result = BRepAlgoAPI_Cut(result, gen_3).Shape()

	tr.SetTranslation(gp_Vec(-14,0,0))
	gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
	result = BRepAlgoAPI_Cut(result, gen_3).Shape()

	tr.SetTranslation(gp_Vec(0,-14,0))
	gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
	result = BRepAlgoAPI_Cut(result, gen_3).Shape()

	trns = gp_Trsf()
	trns.SetTranslation(gp_Vec(0,0,+10))

	gen_2 = BRepBuilderAPI_Transform(result, trns).Shape()


	rou = BRepPrimAPI_MakeCylinder(2, 20).Shape()
	trns = gp_Trsf()
	trns.SetTranslation(gp_Vec(0,0,+15))

	gen_3 = BRepBuilderAPI_Transform(rou, trns).Shape()

	result = BRepAlgoAPI_Fuse(center, gen_1).Shape()
	result = BRepAlgoAPI_Fuse(gen_2, result).Shape()
	result = BRepAlgoAPI_Fuse(gen_3, result).Shape()

	step_writer = STEPControl_Writer()
	Interface_Static_SetCVal("write.step.schema", "AP203")

		# transfer shapes and write file
	step_writer.Transfer(result, STEPControl_AsIs)
	status = step_writer.Write("solid.stp")

	filename = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/solid.stp"
	wrapper = FileWrapper(file(filename))
	response = HttpResponse(wrapper, content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(filename)
	response['Content-Length'] = os.path.getsize(filename)
	return response


def download(request):

	filename = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/stp/motor.stp"
	wrapper = FileWrapper(file(filename))
	response = HttpResponse(wrapper, content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(filename)
	response['Content-Length'] = os.path.getsize(filename)
	return response

def drawsolid(lc):
	my_renderer = x3dom_renderer.X3DomRenderer()
	
	center = BRepPrimAPI_MakeCylinder(13, 15).Shape()

	cyl_1 = BRepPrimAPI_MakeCylinder(15, 5).Shape()
	trns = gp_Trsf()
	trns.SetTranslation(gp_Vec(0,0,-5))

	gen_1 = BRepBuilderAPI_Transform(cyl_1, trns).Shape()

	cyl_2 = BRepPrimAPI_MakeCylinder(15, 5).Shape()

	trou = BRepPrimAPI_MakeCylinder(1, 5).Shape()
	tr = gp_Trsf()
	tr.SetTranslation(gp_Vec(0,14,0))
	gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
	result = BRepAlgoAPI_Cut(cyl_2, gen_3).Shape()

	tr.SetTranslation(gp_Vec(14,0,0))
	gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
	result = BRepAlgoAPI_Cut(result, gen_3).Shape()

	tr.SetTranslation(gp_Vec(-14,0,0))
	gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
	result = BRepAlgoAPI_Cut(result, gen_3).Shape()

	tr.SetTranslation(gp_Vec(0,-14,0))
	gen_3 = BRepBuilderAPI_Transform(trou, tr).Shape()
	result = BRepAlgoAPI_Cut(result, gen_3).Shape()


	trns = gp_Trsf()
	trns.SetTranslation(gp_Vec(0,0,+10))

	gen_2 = BRepBuilderAPI_Transform(result, trns).Shape()



	rou = BRepPrimAPI_MakeCylinder(2, 20).Shape()
	trns = gp_Trsf()
	trns.SetTranslation(gp_Vec(0,0,+15))

	gen_3 = BRepBuilderAPI_Transform(rou, trns).Shape()

	result = BRepAlgoAPI_Fuse(center, gen_1).Shape()
	result = BRepAlgoAPI_Fuse(gen_2, result).Shape()
	result = BRepAlgoAPI_Fuse(gen_3, result).Shape()

	dis = my_renderer.DisplayShape(result)
	
	return dis