from __future__ import unicode_literals
from django import forms
from django.db import models

# Create your models here.

class updat(forms.Form):
	lc = forms.CharField(label='', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Longueur Cylindre ', 'value' : '15'}))
	rc = forms.CharField(label='', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Rayon Cylindre ', 'value' : ''}))
	rcm = forms.CharField(label='', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Rayon Cylindre milieu ', 'value' : ''}))
	lcm = forms.CharField(label='', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Longueur Cylindre milieu ', 'value' : ''}))
	lb = forms.CharField(label='', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Longueur bras ', 'value' : ''}))
	nt = forms.CharField(label='', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Nbr de trou ', 'value' : ''}))
	rt = forms.CharField(label='', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Rayon du trou ', 'value' : ''}))
	